var path = require("path");
var htmlWebpackPlugin = require('html-webpack-plugin');
var htmlIncluderWebpackPlugin = require('html-includer-webpack-plugin').default;




module.exports = {
  entry: './src/index.js',

  output: {
    path: path.resolve(__dirname + '/dist'),
    filename: '[name].js',
  },

  resolve: {
    modules: [path.resolve(__dirname, 'src'), "node_modules", "bower_components" ],
    descriptionFiles: ['package.json', 'bower.json']
  },

  plugins: [new htmlWebpackPlugin({
    template: "./src/index_template.ejs"
  }),new htmlIncluderWebpackPlugin()], 

  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [
          'style-loader',
          'css-loader',
        ]
      },
      {
        test:    /\.html$/,
        exclude: /node_modules/,
        loader:  'file-loader?name=[name].[ext]',
      },
      {
        test:    /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        loader:  'elm-webpack-loader?verbose=true&warn=true',
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff',
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
      },
    ],

    noParse: /\.elm$/,
  },

  devServer: {
    inline: true,
    stats: { colors: true },
  },


};