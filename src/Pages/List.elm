module Pages.List exposing (..)

import Html exposing (..)
import Html.Attributes exposing (class, value, href, src, attribute, style, id)
import Html.Events exposing (onClick)
import Json.Encode exposing (..)
import Models exposing (Model, Depiction, Emotion, EmotionHistory, EmotionChart)
import Msgs
import RemoteData
import Routing exposing (boothPath)


view : Model -> List Depiction -> Html Msgs.Msg
view model depictions =
    let
        qualifier =
            "data:image/jpeg;base64,"

        picture =
            qualifier ++ model.imageSrc

        faces =
            List.map (\depiction -> depiction.depictionAttributes.emotions) depictions

        theCharts faces emotionHistory =
            List.map
                (\face ->
                    chart face emotionHistory
                )
                faces

        emotion =
            case model.emotions of
                RemoteData.NotAsked ->
                    text ""

                RemoteData.Loading ->
                    text "Loading..."

                RemoteData.Success emotions ->
                    div [ class "overflow-hidden" ] (theCharts faces emotions)

                RemoteData.Failure err ->
                    text (toString err)
    in
        div []
            [ header [ class "center" ]
                [ img
                    [ id "logo"
                    , src "https://s3-us-west-2.amazonaws.com/www.onprem.com/assets/img/original+images/onprem_logo.png"
                    ]
                    []
                , div [ class "h6" ] [ text "***powered by Amazon Rekognition***" ]
                ]
            , div [] []
            , div
                [ class "clearfix mb2 center pt3" ]
                [ div [ class "left mr1 ml4" ]
                    [ img [ src picture, class "rounded" ] []
                    ]

                --, div [ class "overflow-hidden" ] (List.map viewCurrentEmotion depictions)
                , div [ class "overflow-hidden", id "EmotionsAtNAB" ] [ text "Emotions at NAB" ]
                , emotion

                --, div [ class "overflow-hidden" ] (List.map viewDepictionAttributesResult depictions)
                ]
            , div [ class "center", style [ ( "clear", "both" ) ] ] [ boothBtn ]
            , div [ class "h6 center" ] [ text "© 2017 | OnPrem Solution Partners, LLC" ]
            ]


boothBtn : Html Msgs.Msg
boothBtn =
    a
        [ class "btn regular center"
        , href boothPath
        ]
        [ button [ class "button center", onClick Msgs.GoHome ] [ text "Try Again" ]
        ]


googleChart : List (Attribute a) -> List (Html a) -> Html a
googleChart =
    Html.node "google-chart"


title : String -> Value
title title =
    Json.Encode.object
        [ ( "title", string title )
        , ( "bars", string "horizontal" )
        , ( "animation", Json.Encode.object [ ( "duration", string "5000" ) ] )
        , ( "colors", Json.Encode.list [ (Json.Encode.string "#61a3df"), (Json.Encode.string "#193f78"), (Json.Encode.string "#ababab") ] )
        ]


colData : String -> String -> Value
colData label labelType =
    Json.Encode.object
        [ ( "label", string label )
        , ( "type", string labelType )
        ]


cols : Value
cols =
    Json.Encode.list
        [ (colData "Emotion" "string"), (colData "You" "number"), (colData "High" "number"), (colData "Average" "number") ]


rowData : EmotionChart -> Value
rowData emotion =
    Json.Encode.list
        [ (Json.Encode.string emotion.emotion), (Json.Encode.float emotion.current), (Json.Encode.float emotion.high), (Json.Encode.float emotion.average) ]


row : List EmotionChart -> Value
row emotions =
    Json.Encode.list
        (List.map rowData emotions)


chart : List Emotion -> List EmotionHistory -> Html msg
chart emotions history =
    googleChart
        [ attribute "type" "md-bar"
        , attribute "options" (Json.Encode.encode 0 <| title "test title")
        , attribute "cols" (Json.Encode.encode 0 <| cols)
        , attribute "rows" (Json.Encode.encode 0 <| row (process emotions history))
        , style [ ( "width", "90%" ) ]
        , class "graphBorder"
        ]
        []


process : List Emotion -> List EmotionHistory -> List EmotionChart
process emotions histories =
    let
        mergeEmotionAndHistory emotion history =
            { emotion = emotion.emotion
            , current = emotion.confidence
            , high = history.high
            , average = history.average
            }

        defaultCaseHistory history =
            { emotion = history.emotion
            , current = 0
            , high = history.high
            , average = history.average
            }

        findMatchingEmotion history =
            emotions |> List.filter (\emotion -> emotion.emotion == history.emotion) |> List.head
    in
        histories
            |> List.map
                (\history ->
                    case findMatchingEmotion history of
                        Just emotion ->
                            mergeEmotionAndHistory emotion history

                        Nothing ->
                            defaultCaseHistory history
                )
