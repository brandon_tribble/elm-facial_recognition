module Pages.Booth exposing (..)

import Html exposing (..)
import Html.Attributes exposing (class, href, style, src, autoplay, id, title)
import Html.Events exposing (onMouseOver)
import Msgs exposing (Msg)
import Models exposing (Model, Depiction)
import Routing exposing (resultsPath)


view : Model -> Html Msg
view model =
    div
        [ class "center" ]
        [ header [ class "center" ]
            [ img
                [ id "logo"
                , src "https://s3-us-west-2.amazonaws.com/www.onprem.com/assets/img/original+images/onprem_logo.png"
                ]
                []
            , div [ class "h6" ] [ text "***powered by Amazon Rekognition***" ]
            ]
        , div [ class "faceText" ] [ text "Make a Face!" ]
        , div [ class "photobooth center" ]
            [ div [ class "controls center" ]
                [ video [ class "player display-none", src model.videoSrc, autoplay True, style [ ( "width", "640" ), ( "height", "480" ) ] ] []
                , canvas [ class "photo center p2", style [ ( "width", "640" ), ( "height", "480" ) ] ] []
                , div [ class "mask center" ] []
                ]
            ]
        , div []
            [ div [ class "inline-block emotionCue", title "Happy" ] [ text "😄" ]
            , div [ class "inline-block emotionCue", title "Sad" ] [ text "😰" ]
            , div [ class "inline-block emotionCue", title "Angry" ] [ text "😡" ]
            , div [ class "inline-block" ] [ resultsBtn ]
            , div [ class "inline-block emotionCue", title "Disgusted" ] [ text "😁" ]
            , div [ class "inline-block emotionCue", title "Surprised" ] [ text "😯" ]
            , div [ class "inline-block emotionCue", title "Calm" ] [ text "😌" ]
            ]
        , div [ id "footer", class "footer h6 italic center" ] [ text "© 2017 | OnPrem Solution Partners, LLC" ]
        ]


resultsBtn : Html msg
resultsBtn =
    a
        [ class "btn regular center pt2"
        , href resultsPath
        ]
        [ button [ class "button" ] [ text "Take Photo" ]
        ]
