module Routing exposing (..)

import Models exposing (Route(..))
import Navigation exposing (Location)
import UrlParser exposing (..)


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map PhotoBooth top
        , map Results (s "results")
        , map PhotoBooth (s "booth")
        , map About (s "about")
        ]


parseLocation : Location -> Route
parseLocation location =
    case (parseHash matchers location) of
        Just route ->
            route

        Nothing ->
            NoResults


resultsPath : String
resultsPath =
    "#results"


boothPath : String
boothPath =
    "#booth"


aboutPath : String
aboutPath =
    "#about"
