module View exposing (..)

import Html exposing (Html, div, text)
import Msgs exposing (Msg)
import Models exposing (Model)
import Pages.List
import Pages.Booth
import RemoteData


view : Model -> Html Msg
view model =
    div []
        [ page model ]


page : Model -> Html Msg
page model =
    case model.route of
        Models.PhotoBooth ->
            Pages.Booth.view model

        Models.Results ->
            userAttributesPage model

        Models.About ->
            Pages.Booth.view model

        Models.NoResults ->
            notFoundView


userAttributesPage : Model -> Html Msg
userAttributesPage model =
    case model.depictions of
        RemoteData.NotAsked ->
            text ""

        RemoteData.Loading ->
            text "Loading..."

        RemoteData.Success depictions ->
            Pages.List.view model depictions

        RemoteData.Failure err ->
            text (toString err)


notFoundView : Html msg
notFoundView =
    div []
        [ text "Not Found"
        ]
