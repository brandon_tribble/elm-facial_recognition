'use strict';

require('ace-css/css/ace.css');
require('font-awesome/css/font-awesome.css');
require('./styles.css');


// Require index.html so it gets copied to dist
// require('./index.html');
require('!include-loader!google-chart')

var Elm = require('./Main.elm');
var mountNode = document.getElementById('main');

// .embed() can take an optional second argument. This would be an object describing the data we need to start a program, i.e. a userID or some token
var app = Elm.Main.embed(mountNode);
    
function getVideo() {
    navigator.mediaDevices.getUserMedia({ video: true, audio: false })
        .then(function (localMediaStream) {
        var src = window.URL.createObjectURL(localMediaStream);
        app.ports.videoSrc.send(src);
        paintToCanavas();
    })["catch"](function (err) {
        console.error("OH NO!!!", err);
    });
}     
    
function paintToCanavas() {
    var shutter = document.querySelector('#main .button');
    var video = document.querySelector('#main .player');
    var canvas = document.querySelector('#main .photo');
    var ctx = canvas.getContext('2d');
    var width = 640; 
    var height = 480;
    canvas.width = width;
    canvas.height = height;
    drawImage();
    shutter.addEventListener('click', takePhoto)

    function drawImage () {
        return setInterval(function () {
            ctx.drawImage(video, 0, 0, width, height);
        }, 16);
    }

    function takePhoto() {    
        var data = canvas.toDataURL('image/jpeg');
        var imgSrc = data.slice(23);
        app.ports.pictureData.send(imgSrc);
    }

    app.ports.pictureTaken.subscribe(takePhoto);

}
    

app.ports.videoRolling.subscribe(getVideo)



//setTimeout(getVideo, 1000);
requestAnimationFrame(getVideo)



