module Update exposing (..)

import Commands exposing (loadBooth, postDepictionsCmd, getEmotionsCmd)
import Msgs exposing (Msg)
import Models exposing (Model, Depiction)
import Routing exposing (parseLocation)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Msgs.OnLocationChange location ->
            let
                newRoute =
                    parseLocation location
            in
                ( { model | route = newRoute }, Cmd.none )

        Msgs.OnGetDepictions depictions ->
            ( { model | depictions = depictions }, getEmotionsCmd )

        Msgs.OnGetEmotions emotions ->
            ( { model | emotions = emotions }, Cmd.none )

        Msgs.PhotoTaken imageSrc ->
            ( { model | imageSrc = imageSrc }, postDepictionsCmd imageSrc )

        Msgs.PhotoFailed error ->
            ( { model | errorMessage = Maybe.Just error }, Cmd.none )

        Msgs.VideoPlaying videoSrc ->
            ( { model | videoSrc = videoSrc }, Cmd.none )

        Msgs.VideoFailed error ->
            ( { model | errorMessage = Maybe.Just error }, Cmd.none )

        Msgs.GoHome ->
            ( model, loadBooth )
