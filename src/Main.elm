port module Main exposing (..)

import Commands exposing (decodeImgSrc, decodeVideoSrc)
import Json.Decode as Decode
import Models exposing (Model, initialModel)
import Navigation exposing (Location)
import Routing
import Update exposing (update)
import View exposing (view)
import Msgs exposing (Msg)


init : Location -> ( Model, Cmd Msg )
init location =
    let
        currentRoute =
            Routing.parseLocation location
    in
        ( initialModel currentRoute, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ pictureData decodeImgSrc
        , videoSrc decodeVideoSrc
        ]



-- MAIN


main : Program Never Model Msg
main =
    Navigation.program Msgs.OnLocationChange
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


port pictureTaken : String -> Cmd msg


port pictureData : (Decode.Value -> msg) -> Sub msg


port videoRolling : String -> Cmd msg


port videoSrc : (Decode.Value -> msg) -> Sub msg
