module Models exposing (..)

import RemoteData exposing (WebData)


initialModel :
    route
    ->
        { depictions : WebData (List Depiction)
        , emotions : WebData (List EmotionHistory)
        , emotionsChart : List EmotionChart
        , errorMessage : Maybe String
        , imageSrc : String
        , route : route
        , videoSrc : String
        }
initialModel route =
    { imageSrc = ""
    , videoSrc = ""
    , depictions = RemoteData.Loading
    , emotions = RemoteData.Loading
    , emotionsChart = []
    , errorMessage = Nothing
    , route = route
    }


type alias Model =
    { imageSrc : ImageSource
    , videoSrc : VideoSource
    , depictions : WebData (List Depiction)
    , emotions : WebData (List EmotionHistory)
    , emotionsChart : List EmotionChart
    , errorMessage : Maybe String
    , route : Route
    }


type alias ImageSource =
    String


type alias VideoSource =
    String


type alias Depiction =
    { depictionId : DepictionId
    , depictionAttributes : DepictionAttributes
    }


type alias DepictionId =
    String


type alias DepictionAttributes =
    { mustache : AttributeScore
    , eyeglasses : AttributeScore
    , mouthOpen : AttributeScore
    , eyesOpen : AttributeScore
    , smile : AttributeScore
    , sunglasses : AttributeScore
    , emotions : List Emotion
    }


type alias EmotionAttributes =
    List Emotion


type alias AttributeScore =
    { value : Bool
    , confidence : Float
    }


type alias Emotion =
    { emotion : String
    , confidence : Float
    }


type alias EmotionHistory =
    { emotion : String
    , high : Float
    , average : Float
    }


type alias EmotionChart =
    { emotion : String
    , current : Float
    , high : Float
    , average : Float
    }


type Route
    = PhotoBooth
    | Results
    | About
    | NoResults
