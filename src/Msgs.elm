module Msgs exposing (..)

import Models exposing (Depiction, ImageSource, VideoSource, EmotionHistory)
import Navigation exposing (Location)
import RemoteData exposing (WebData)


type Msg
    = OnGetDepictions (WebData (List Depiction))
    | OnGetEmotions (WebData (List EmotionHistory))
    | OnLocationChange Location
    | PhotoTaken ImageSource
    | PhotoFailed String
    | VideoPlaying VideoSource
    | VideoFailed String
    | GoHome
