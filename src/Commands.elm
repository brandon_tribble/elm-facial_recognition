module Commands exposing (..)

import Http
import Json.Decode as Decode
import Json.Decode.Pipeline exposing (decode, required)
import Json.Encode as Encode
import Msgs exposing (Msg)
import Models exposing (Depiction, ImageSource, DepictionAttributes, AttributeScore, Emotion, EmotionHistory)
import Navigation exposing (load)
import RemoteData


getDepictionsRequest : ImageSource -> Http.Request (List Depiction)
getDepictionsRequest image =
    Http.post getDepictionsUrl (getDepictionsBody image) imageResponseDeocder


postDepictionsRequest : ImageSource -> Http.Request (List Depiction)
postDepictionsRequest image =
    getDepictionsPost getDepictionsUrl (getDepictionsBody image)


getEmotionsRequest : Http.Request (List EmotionHistory)
getEmotionsRequest =
    getEmotions getEmotionsUrl


getDepictionsUrl : String
getDepictionsUrl =
    "https://nab.rekog.demo.onprem.ninja:18080/depictions/detect"


getEmotionsUrl : String
getEmotionsUrl =
    "https://nab.rekog.demo.onprem.ninja:18080/depictions/emotions"


getDepictionsPost : String -> Http.Body -> Http.Request (List Depiction)
getDepictionsPost url body =
    Http.request
        { method = "POST"
        , headers = []
        , url = url
        , body = body
        , expect = Http.expectJson imageResponseDeocder
        , timeout = Nothing
        , withCredentials = False
        }


getEmotions : String -> Http.Request (List EmotionHistory)
getEmotions url =
    Http.request
        { method = "GET"
        , headers = []
        , url = url
        , body = Http.emptyBody
        , expect = Http.expectJson emotionsResponseDecoder
        , timeout = Nothing
        , withCredentials = False
        }


getDepictionsBody : ImageSource -> Http.Body
getDepictionsBody image =
    Http.jsonBody <|
        Encode.object
            [ ( "imageSource"
              , Encode.object
                    [ ( "sourceType", Encode.string "ImageBinary" )
                    , ( "source", Encode.string image )
                    ]
              )
            ]


imageResponseDeocder : Decode.Decoder (List Depiction)
imageResponseDeocder =
    --list depictionDecoder
    Decode.at [ "depictions" ] (Decode.list depictionDecoder)


depictionDecoder : Decode.Decoder Depiction
depictionDecoder =
    decode Depiction
        |> required "id" Decode.string
        |> required "depictionAttributes" attributesDecoder


attributesDecoder : Decode.Decoder DepictionAttributes
attributesDecoder =
    decode DepictionAttributes
        |> required "mustache" attributeScoreDecoder
        |> required "eyeglasses" attributeScoreDecoder
        |> required "mouthOpen" attributeScoreDecoder
        |> required "eyesOpen" attributeScoreDecoder
        |> required "smile" attributeScoreDecoder
        |> required "sunglasses" attributeScoreDecoder
        |> required "emotions" emotionsDecoder


attributeScoreDecoder : Decode.Decoder AttributeScore
attributeScoreDecoder =
    decode AttributeScore
        |> required "value" Decode.bool
        |> required "confidence" Decode.float


emotionsDecoder : Decode.Decoder (List Emotion)
emotionsDecoder =
    Decode.at [ "emotionAttributes" ] (Decode.list emotionScoreDecoder)


emotionScoreDecoder : Decode.Decoder Emotion
emotionScoreDecoder =
    decode Emotion
        |> required "emotion" Decode.string
        |> required "confidence" Decode.float


emotionsResponseDecoder : Decode.Decoder (List EmotionHistory)
emotionsResponseDecoder =
    Decode.at [ "emotions" ] (Decode.list emotionHistoryDecoder)


emotionHistoryDecoder : Decode.Decoder EmotionHistory
emotionHistoryDecoder =
    decode EmotionHistory
        |> required "emotion" Decode.string
        |> required "high" Decode.float
        |> required "average" Decode.float


postDepictionsCmd : ImageSource -> Cmd Msg
postDepictionsCmd image =
    postDepictionsRequest image
        |> RemoteData.sendRequest
        |> Cmd.map Msgs.OnGetDepictions


getEmotionsCmd : Cmd Msg
getEmotionsCmd =
    getEmotionsRequest
        |> RemoteData.sendRequest
        |> Cmd.map Msgs.OnGetEmotions



-- For getting a fresh load when you want to 'Try Again'


loadBooth : Cmd Msg
loadBooth =
    load "https://nab.rekog.demo.onprem.ninja"



-- Subscription Decoders


imageDecoder : Decode.Decoder String
imageDecoder =
    Decode.string


decodeImgSrc : Decode.Value -> Msg
decodeImgSrc image =
    case Decode.decodeValue imageDecoder image of
        Ok imageSrc ->
            Msgs.PhotoTaken imageSrc

        Err errorMessage ->
            Msgs.PhotoFailed errorMessage


decodeVideoSrc : Decode.Value -> Msg
decodeVideoSrc video =
    case Decode.decodeValue imageDecoder video of
        Ok videoSrc ->
            Msgs.VideoPlaying videoSrc

        Err errorMessage ->
            Msgs.VideoFailed errorMessage
